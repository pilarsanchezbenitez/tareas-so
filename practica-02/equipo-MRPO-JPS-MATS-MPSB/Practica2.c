#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/moduleparam.h>
#include <linux/stat.h>
#include <linux/fs.h>
#include <linux/uaccess.h>

// Nombre del módulo y el archivo en /dev asociado a este
#define DEV_NAME "Practica2"

static unsigned int entero;
module_param(entero, int, 0);
MODULE_PARM_DESC(entero, "Número entero de 8 bits");

static char cadenap[32] = "";
module_param_string(cadena, cadenap, 32, 0);
MODULE_PARM_DESC(cadenap, "Arreglo de caracteres de maximo 32 elementos");

int arreglo[5];
int sizeee;
module_param_array(arreglo, int, &sizeee, 0);
MODULE_PARM_DESC(arreglo, "Arreglo de maximo 5 enteros");



/* device_open - informa al kernel que se abrió el archivo asociado al driver
 * y evita de esta forma que el modulo sea eliminado del kernel usando rmmod
 * mientras el archivo este abierto.
 * NOTA: Copia la firma (tipo de retorno y el tipo de parametros) para tú 
 * practica.
 */
static int device_open (struct inode *i, struct file *f) {
    printk(KERN_INFO DEV_NAME ": Dispositivo abierto");
    // Esta función informa al kernel que se esta usando el módulo, por cada una
    // de las llamadas a esta función debe haber una llamada a module_put
    try_module_get(THIS_MODULE);
    return 0;
}

/* device_release -  informa al kernel que el archivo asociado al driver ya no
 * esta en uso.
 * NOTA: Copia la firma (tipo de retorno y el tipo de parametros) para tú 
 * practica.
 */
static int device_release(struct inode *i, struct file *f) {
    printk(KERN_INFO DEV_NAME ": Dispositivo cerrado");
    module_put(THIS_MODULE);
    return 0;
}


/* device_read - Copia el mensaje a un buffer en espacio de usuario.
 * NOTA: La implementación de esta función en tu práctica debe cuidar que no se
 * copien más bytes de los indicados por el parámetro count. Copia la firma de 
 * la función (tipo de retorno y el tipo de parametros) para tú practica.
 */
static ssize_t device_read(struct file *file, char __user *buf, size_t count, loff_t *o) {

    int res = 0;

    if (entero != 0) {
        size_t len = sizeof(int);
        while (count > len) {
            copy_to_user(buf, &entero, len);
            buf++;
            count -= len;
            res++;
        }
    } else if (strlen(cadenap)) {
        size_t len = strlen(cadenap);
        while (count > len) {
            copy_to_user(buf, cadenap, len);
            buf +=  len;
            count -= len;
            res += len;
        }
    } else if (arreglo[0] || arreglo[1] || arreglo[2] || arreglo[3] || arreglo[4]) {
        size_t len =  sizeof(int);
        int i = 0;
        while (count > len) {
            copy_to_user(buf, &arreglo[i], len);
            buf++;
            count -= len;
            res += len;
            i= (i + 1) % 5;
        }        
    }

    return res;
}

/* device_write - Devuelve error al intentar abrir el archivo asociado a este 
 * modulo para escritura.
 * NOTA: Copia la firma (tipo de retorno y el tipo de parametros) para tú 
 * practica.
 */
ssize_t device_write(struct file *f, const char *c, size_t s, loff_t *o) {
    return -EINVAL;
}

// Operaciones soportadas por el archivo en /dev 
static struct file_operations fops = {
    .owner = THIS_MODULE,
    .open = device_open,
    .release = device_release,
    .write = device_write,
    .read = device_read
};

// Numero de dispositivo asociado al archivo en /dev
static int major;

int __init mod_init(void) {
    // Registramos un nuevo dispositivo de caracteres en el kernel
    major = register_chrdev(0, DEV_NAME, &fops);

    if (major < 0) {
        printk(KERN_ALERT DEV_NAME ": Error al registrar un nuevo dispositivo");
        return major;
    }
       
    printk(KERN_NOTICE DEV_NAME ": Numero mayor: %d", major);

    return 0;
}

void __exit mod_cleanup(void) {
    if (major > 0) {
        printk(KERN_INFO "Adios mundo!\n");
        unregister_chrdev(major, DEV_NAME);
    }
    return;
}

module_init(mod_init);
module_exit(mod_cleanup);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Mauricio Riva Palacio");
MODULE_AUTHOR("Jacqueline Paredes");
MODULE_AUTHOR("Miguel A. Torres");
MODULE_AUTHOR("Maria del Pilar Sanchez");
MODULE_DESCRIPTION("Practica 2");
