# Universidad Nacional Áutonoma de México
# Facultad de Ciencias
## Practica 02. Módulo del kernel de Linux

## Equipo:
* Mauricio Riva Palacio Orozco      mauricioriva@ciencias.unam.mx
* Jacqueline Paredes Sanchez        jacqueline.pa@ciencias.unam.mx
* Miguel Angel Torres Sanchez       miguel.atorress@ciencias.unam.mx
* Maria del Pilar Sanchez Benitez   pilar_sb_cc@ciencias.unam.mx
## Prerequisitos:
Tener instalado el compilador de C biuld-essential
Tener un kernel compatible
### ¿Como compilar y ejecutar?

Descargar el repositorio https://gitlab.com/pilarsanchezbenitez/tareas-so.git y situarse en la carpeta: tareas-so/Practica-02/equipo-MRPO-JPS-MATS-MPSB

1. Compilamos el codigo usando el comando `make` comprobando que estemos dentro de la carpeta
2. Insertamos el modulo en el kernel con el comando con los parametros a probar `sudo insmod ./src/Parctica2.ko` y para probar cada parametro usamos 
    * `sudo insmod ./src/Parctica2.ko entero= 7`
    * `sudo insmod ./src/Parctica2.ko cadena="Hola mundo"`
    * `sudo insmod ./src/Parctica2.ko arreglo=1,3,5,6,7`

3. Copiamos el número mayor que se imprime en el log del kernel
4. Para observar que se imprimio  ejecutamos `demesg | tail`
5. Usando el número crea un nuevo archivo en /dev `$ sudo mknod /dev/Practica2c <numero_mayor>0` 
6. Usa `cat` para leer del archivo `$ cat /dev/Practica2`
7. Hacer Ctrl+C
8. leer salida `hexdump -c salida`

9. Utilizamos `$ sudo rmmod Practica2` para eliminar el modulo creado
10. Utilizamos `$ sudo rm/dev/Practica2 salida` para eliminar los archivos dev y salida

### Caracteristicas del modulo:
* El modulo puede tomar 3 posibles parametros `entero`, `cadena` y `arreglo`
* La función read copia tantas veces como sea posible en el buffer el parametro de mayor prioridad
* * En primer lugar se toma el entero, si este es vacio, tomamos la cadena y si tambien es vacio, se toma el arrglo
### Bugs conocidos: 
