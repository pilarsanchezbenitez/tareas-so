# Universidad Nacional Autónoma de México
# Facultad de Ciencias
# Ciencias de la Computación

## Sistemas Operativos

Semestre 2021-2

En este repositorio estaremos manejando las tareas de la materia de [sistemas operativos][url-sistemas_operativos-gitlab] que se imparte en la [Facultad de Ciencias, UNAM][url-sistemas_operativos-fciencias] en el semestre 2021-2.

### Estructura de directorios

+ Cada alumno deberá tener una carpeta dentro de la que hará entrega de sus tareas y prácticas
+ Si la práctica es en equipo, uno sube la carpeta de la práctica indicando en el archivo `README.md` el nombre de los integrantes del equipo.
+ Cada tarea o práctica deberá tener lo siguiente:
    * Un archivo `README.md` donde se entregará la documentación en formato _markdown_. Se puede hacer uso de un directorio `img/` para guardar las imágenes o capturas necesarias para la documentación
    * Un archivo `Makefile` que servirá para compilar el programa y hacer pruebas

```
/AndresHernandez/
├── README.md
├── tareas
│   ├── tarea-0/
│   │   ├── img/
│   │   │   └── ...
│   │   ├── README.md
│   │   └── ...
│   ├── tarea-1/
│   │   ├── img/
│   │   │   └── ...
│   │   ├── README.md
│   │   ├── Makefile
│   │   ├── programa.c
│   │   └── ...
│   ├── ...
│   └── tarea-n/
│   │   ├── img/
│   │   │   └── ...
│   │   ├── README.md
│   │   └── ...
└── practicas/
    ├── practica-1/
    │   ├── img/
    │   │   └── ...
    │   ├── README.md
    │   └── ...
    ├── practica-2/
    ├── ...
    └── practica-m/
        ├── img/
        │   └── ...
        ├── README.md
        └── ...
```

--------------------------------------------------------------------------------

### Flujo de trabajo para la entrega de tareas

![https://drive.google.com/open?id=1_fhDH4NX_2kvMWoRupcdpaOqAATsbw9s](img/000-workflow.png "")

### Acceder al repositorio principal

+ Abre la URL del [repositorio de tareas para la materia][repositorio-tareas]

|                                       |
|:-------------------------------------:|
| ![](img/001-Main_repo.png "") |

+ [Inicia sesión en GitLab][gitlab-login]

|                                     |
|:-----------------------------------:|
| ![](img/002-Sign_in.png "") |

--------------------------------------------------------------------------------

#### Hacer `fork` al repositorio principal

+ Da clic en el botón `fork` para crear una copia del repositorio `tareas-so` en tu cuenta de usuario

|                                       |
|:-------------------------------------:|
| ![](img/003-Fork_repo.png "") |

+ Espera a que el __fork__ se complete

|                                              |
|:--------------------------------------------:|
| ![](img/004-Fork_in_progress.png "") |

--------------------------------------------------------------------------------

#### Clonar el repositorio

+ Accede a la URL del repositorio `tareas-so` asociado a **tu cuenta de usuario**

```
https://gitlab.com/USUARIO/tareas-so.git
```

+ Obten la URL de tu repositorio `tareas-so` y bájalo a tu equipo con `git clone`:

|                                                       |
|:-----------------------------------------------------:|
| ![](img/005-Fork_successful-clone_URL.png "") |

```
$ git clone https://gitlab.com/USUARIO/tareas-so.git
Cloning into 'tareas-so'...
remote: Enumerating objects: 60, done.
remote: Counting objects: 100% (60/60), done.
remote: Compressing objects: 100% (39/39), done.
remote: Total 171 (delta 19), reused 59 (delta 19), pack-reused 111
Receiving objects: 100% (171/171), 3.38 MiB | 5.41 MiB/s, done.
Resolving deltas: 100% (25/25), done.
Checking connectivity... done.
```

+ Lista el contenido

```
$ cd tareas-so/
$ ls -lA
total 52
drwxr-xr-x 8 tonejito users  4096 Sep 28 01:41 .git
drwxr-xr-x 2 tonejito users  4096 Sep 28 01:41 img
-rw-r--r-- 1 tonejito users 12265 Sep 28 01:41 README.md
```

--------------------------------------------------------------------------------

#### Crear rama personal

+ Crea una rama con tu nombre para versionar tus cambios

```
$ git checkout -b AndresHernandez
Switched to a new branch 'AndresHernandez'
```

+ Comprueba que te encuentres en la rama con tu nombre. Debe tener el prefijo `*`

```
$ git branch
* AndresHernandez
  entregas
```

--------------------------------------------------------------------------------

#### Agregar carpeta personal

+ Accede al repositorio y crea una carpeta con tu nombre

```
$ mkdir -v AndresHernandez
mkdir: created directory ‘AndresHernandez’
```

>>>
Dentro de esa carpeta es donde debes poner los archivos de **TODAS** las tareas que hagas
>>>

--------------------------------------------------------------------------------

##### Agregar archivo con tu nombre

+ Crea un archivo de Markdown que contenga tu nombre

>>>
Observa el _contenido de ejemplo_ en el siguiente paso
>>>

```
$ editor AndresHernandez/README.md
```

+ Contenido de ejemplo para el archivo README.md (incluye los guiones `---` en el contenido del archivo)

```
---
title: NOMBRE COMPLETO
subtitle: NÚMERO DE CUENTA
date: 2019-08-05
---

Esta es la carpeta de NOMBRE COMPLETO
```

--------------------------------------------------------------------------------

#### Enviar cambios al repositorio

+ Una vez que hayas creado los archivos, revisa el estado del repositorio

```
$ git status
On branch master
Your branch is up-to-date with 'origin/master'.
Untracked files:
  (use "git add <file>..." to include in what will be committed)

	AndresHernandez/

nothing added to commit but untracked files present (use "git add" to track)
```

+ Lista el directorio y agrega los archivos con `git add`

```
$ cd AndresHernandez
$ ls -lA
total 4
-rw-rw-r-- 1 tonejito users   0 Sep 28 20:00 .gitkeep
-rw-rw-r-- 1 tonejito users 145 Sep 28 20:00 README.md

$ git add README.md
```

+ Versiona los archivos con `git commit`

>>>
Usa **comillas simples** para especificar el mensaje del _commit_
>>>

```
$ git commit -m 'Carpeta de Andrés Hernández'
[master 97c54ea] Carpeta de Andrés Hernández
 1 files changed, 8 insertions(+)
 create mode 100644 content/tarea/AndresHernandez/README.md
```

+ Revisa que el _remote_ apunte a **tu repositorio** con `git remote`

```
$ git remote -v
origin	https://gitlab.com/USUARIO/tareas-so.git (fetch)
origin	https://gitlab.com/USUARIO/tareas-so.git (push)
```

+ Revisa la rama en la que estas para enviarla a GitLab

```
$ git branch
* AndresHernandez
  entregas
```

+ Envía los cambios a **tu repositorio** utilizando `git push`

```
$ git push -u origin AndresHernandez
Username for 'https://gitlab.com': USUARIO
Password for 'https://USUARIO@gitlab.com':
Counting objects: 6, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (6/6), done.
Writing objects: 100% (6/6), 610 bytes | 610.00 KiB/s, done.
Total 6 (delta 2), reused 0 (delta 0)
remote:
remote: To create a merge request for AndresHernandez, visit:
remote:   https://gitlab.com/USUARIO/tareas-so/merge_requests/new?merge_request%5Bsource_branch%5D=AndresHernandez
remote:
To https://gitlab.com/USUARIO/tareas-so.git
 * [new branch]      AndresHernandez -> AndresHernandez
Branch 'AndresHernandez' set up to track remote branch 'AndresHernandez' from 'origin'.
```

--------------------------------------------------------------------------------

#### Crea un `merge request` para entregar tu tarea

Para integrar las tareas de todos se utilizará la funcionalidad `merge request` de GitLab

+ Accede a la url de **tu repositorio**:

```
https://gitlab.com/USUARIO/tareas-so
```

|                                            |
|:------------------------------------------:|
| ![](img/006-Fork-commit_ok.png "") |

+ Selecciona la rama con tu nombre

|                                                |
|:----------------------------------------------:|
| ![](img/007-Fork_select_branch.png "") |

+ Verifica que aparezca el título de tu commit

|                                             |
|:-------------------------------------------:|
| ![](img/008-Fork_new_branch.png "") |

+ Ve a la sección llamada `merge requests` en la barra lateral

+ Crea un nuevo `merge request` para enviar los cambios al repositorio central

|                                         |
|:---------------------------------------:|
| ![](img/009-Fork-new_MR.png "") |

--------------------------------------------------------------------------------

##### Llena los datos del _merge request_

+ Selecciona _la rama con tu nombre_ como **origen** y la rama `tareas-so` como **destino**

|                                                 |
|:-----------------------------------------------:|
| ![](img/010-Fork-MR_data-branch.png "") |

+ Escribe un título y una descripción que sirva como vista previa para tu entrega

+ Da clic en el botón _submit_ para crear tu _merge request_

|                                          |
|:----------------------------------------:|
| ![](img/011-Fork-MR_data.png "") |

--------------------------------------------------------------------------------

##### Notificaciones de creación y seguimiento del MR

+ Una vez que hayas enviado el `merge request`, le llegará un correo electrónico al responsable para que integre tus cambios

|                                             |
|:-------------------------------------------:|
| ![](img/012-Main_MR_created.png "") |

+ Cuando se hayan integrado tus cambios te llegará un correo electrónico de confirmación y aparecerá en el panel del [repositorio principal][repositorio-tareas]

|                                            |
|:------------------------------------------:|
| ![](img/013-Main_MR_merged.png "") |

--------------------------------------------------------------------------------

>>>
**Protip**
Puedes cambiar la visibilidad del proyecto a privada

+ En la barra lateral seleccionar `Settings` > `General`
+ Expande la parte llamada `Permissions`, ahí puedes seleccionar la visibilidad deseada
+ Da clic en `Save changes` para aplicar
>>>

--------------------------------------------------------------------------------

##### Vista en GitLab

+ <https://gitlab.com/SistemasOperativos-Ciencias-UNAM/2021-2/tareas-so>

--------------------------------------------------------------------------------

[url-sistemas_operativos-gitlab]: https://SistemasOperativos-Ciencias-UNAM.gitlab.io/ "Página en GitLab"
[url-sistemas_operativos-fciencias]: http://www.fciencias.unam.mx/docencia/horarios/presentacion/322385 "Sistemas Operativos 2021-2"

[repositorio-tareas]: https://gitlab.com/SistemasOperativos-Ciencias-UNAM/2021-2/tareas-so
[gitlab-login]: https://gitlab.com/users/sign_in
[repositorio-personal]: https://gitlab.com/USUARIO/tareas-so

