# Universidad Nacional Áutonoma de México
# Facultad de Ciencias
## Practica 01.  WebServer

## Equipo:
* Mauricio Riva Palacio Orozco
* Jacqueline Paredes Sanchez
* Miguel Angel Torres Sanchez
* Maria del Pilar Sanchez Benitez
### ¿Como compilar y ejecutar?

1. Dirigirse al directorio practica-1/
1. Ejecutar en la terminal el comando `make` para compilar el servidor.
1. Una vez compilado ejecutar desde la terminal `./server`
1. En la terminal se lanzara el mensaje "Enlazando al puerto"
1. Abrir una terminal nueva y escribir `telnet 0.0.0 8080`
1. Posteriormente podemos escribir peticiones al servidor. 
1. Para eliminar los archivos genrados despues de ejecutar el comando `make` ejecutamos el comando `make clean`.
### Descripción del servidor:
1. Funcinamiento adaptado a multi-proceso
1. Se reciben peticiones GET.
### Bugs conocidos:
