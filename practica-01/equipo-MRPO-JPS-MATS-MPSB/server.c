#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/ip.h>  // INADDR_*
#include <errno.h>
#include<signal.h>
#define PROCESOS 4

void sig(){
 exit(0);
}
// Valida la ruta del archivo, que este exista y no sea superior a la posicion de la ejecucion.
int valid_path(char * path)
{
    // Route upper position
    char *upper_path = strstr(path, "..");
    if(upper_path == NULL) 
    {
        if(access(path, F_OK) == 0) // File exists
        {
            return 0;
        }
        return 1;
    }
    else
    {
        return -1;
    }
}

int main(int argc, char const *argv[])
{
    signal(SIGINT,sig)  ; 
    int proceso = 0;
    int fd = socket(AF_INET, SOCK_STREAM, 0); //File Descriptor
    if (fd == -1) // Error 
    {
        printf("Error al conectarse al socket\n");
        exit(1);
    }
    printf("Socket creado correctamente\n");

    struct sockaddr_in addr;
    addr.sin_family      = AF_INET;      // ipv4
    addr.sin_port        = htons(8081);  // puerto a escuchar
    addr.sin_addr.s_addr = INADDR_ANY;   // escuchar en la ip 0.0.0.0
    
    if (bind(fd, (struct sockaddr *)&addr, sizeof(addr)) == -1) // Le da al socket la direccion local
    {
        printf("Error al agregar la direccion local\n");
        exit(1);
    }
    printf("Enlazado al puerto\n");

    if (-1 == listen(fd, 2)) // Prepara para aceptar conexiones
    {
        printf("Error al enlazar\n");
        // Aqui no termina, debido al numero de conexiones, solo los deja en espera
    }
    printf("Escuchando peticiones\n");

    struct sockaddr_in client_addr = {0};
    socklen_t len                  = sizeof(client_addr);
    int clientfd, rc, c;
    pid_t child_pid;
    char buf[250], buf_aux[250], file_letter;
    char *method, *path, *protocol, *file_content;
    long *file_size;
    FILE *f;

    while (1)
    {
        clientfd = accept(fd, (struct sockaddr *)&client_addr, &len); //Espera una conexion al socket
        if (clientfd == -1)
        {
            printf("Error en la conexion\n");
            exit(1);
        }
        printf("Se ha establecido la conexion\n");
	proceso++;
        if ((child_pid = fork()) == 0){
            c = 0;
            close(fd); //Cerramos el file descriptor
            proceso=proceso-1;
	    if(proceso >= PROCESOS){
		close(clientfd);
		continue;
	    }
            while (1)
            {
                rc = read(clientfd, buf, sizeof(buf) - 1); // Lee lo enviado del cliente y lo guarda en el buffer

                c++; //Maximo 5 peticiones por cliente

                if(rc == -1)
                {
                    printf("Hubo una desconexion");
                    break;
                }

                method = strtok_r(buf, " ", &path); // Obtiene el tipo de metodo
                path = strtok_r(path, " ", &protocol); // Obtiene la ruta del archivo
                
                if ((valid_path(path) == 0) && ((strcmp(method, "GET") == 0) || (strcmp(method, "HEAD") == 0)) /*&& (strcmp(protocol, "HTTP/1.0") == 0)*/ )
                {
                    f = fopen(path, "r"); // Abre el archivo para solo lectura

                    send(clientfd, "\nHTTP/1.0 200 OK\n", 18, 0);
                    send(clientfd, "Content-Type: text/html\n", 25, 0);
                    send(clientfd, "Content-Length: 1324", 22, 0);

                }
                else
                {
                    if (valid_path(path) == 1)
                    {
                        send(clientfd, "\nHTTP/1.0 404 Not Found\n", 25, 0);
                    }
                    else
                    {
                        send(clientfd, "\nHTTP/1.0 400 Bad Request\n", 27, 0);
                    }
                    
                    send(clientfd, "Content-Type: text/html\n", 25, 0);
                    send(clientfd, "Content-Length: 0\n", 19, 0);
                }

                if (c == 5)
                {
                    //Send closed connection header
                    send(clientfd, "Connection: close\n", 19, 0);
                }

                if (strcmp(method, "GET") == 0 && valid_path(path) == 0)
                {
                    
                    while((file_letter = fgetc(f)) != EOF) //Envia caracter por caracter al cliente
                    {
                        buf_aux[0] = file_letter;
                        send(clientfd, buf_aux, sizeof(file_letter), 0);
                    }

                    send(clientfd, "\n", 2, 0);

                    fclose(f);
                }

                if (c == 5)
                {
                    break; //End connection
                }

                bzero(buf, sizeof(buf)); //Limpia el buffer
            }
        }
    }

    // limpiando
    shutdown(fd, SHUT_WR);
    close(clientfd);// cierra tambien el file descriptor
    // close(clientfd);
    shutdown(fd, SHUT_WR);
    close(fd);close(clientfd);
    close(clientfd);

    return 0;
}
