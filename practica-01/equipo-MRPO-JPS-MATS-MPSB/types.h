#ifndef TYPES_H
#define TYPES_H

struct http_response {
  unsigned int code;
  char *status;
  char *mime;
  unsigned int length;
  int clientfd;
  unsigned int connected;
};

#endif